/**
 * Created by evgeny.slastnikov on 9/19/18.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import NumberFormat from 'react-number-format';

import {APP_MODES} from "../configs/params";
import * as FORMATS from "../configs/confirmationPage/formats";
import * as TEXTS from "../configs/confirmationPage/texts";
import {confirmationBtnClick, displayCheckCodeError, tryToConfirm} from "../actions";

class ConfirmationPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            code: ''
        }
    }

    render() {
        return (
            <div className="confirmation-code-block">
                <label htmlFor="confirmationCode" className="confirmation-code-label">
                    {TEXTS.CONFIRMATION}
                    <NumberFormat id="confirmationCode"
                                  className="confirmation-code"
                                  format={FORMATS.CONFIRMATION}
                                  onValueChange={(values) => {
                                      this.setState({code: values.value});
                                      this.props.validateCode(values.value);
                                  }}
                    />
                </label>
                {
                    this.props.errorMessage ?
                    <div className="payment-form-error">{this.props.errorMessage}</div> : ''
                }
                <button className="btn btn-primary" onClick={(e) => {
                    e.preventDefault();
                    this.props.displayCheckCodeError();
                }}>
                    {TEXTS.CONFIRMATION_BUTTON}
                </button>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        errorMessage: state.confirmation.errorMessage
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        validateCode: tryToConfirm,
        confirmationBtnClick: confirmationBtnClick,
        displayCheckCodeError: displayCheckCodeError
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(ConfirmationPage);