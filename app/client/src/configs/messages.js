/**
 * Created by evgeny.slastnikov on 8/6/18.
 */

export const BASIC_ERROR_ALERT = 'Server error. We\'re already working to solve it. Please, try again in 20 minutes';