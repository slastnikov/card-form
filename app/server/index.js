/**
 * Created by evgeny.slastnikov on 8/6/18.
 */

const path = require('path');
const {BASE_DIR, CLIENT_DIR,  APPLICATION_PORT} = require(
    path.join(__dirname, '../..', 'config', 'app.conf')
);

const express = require('express');
const logger = require('morgan');

const app = express();
app.use(logger('dev'));
app.use('/public', express.static(path.join(CLIENT_DIR, 'public')));
app.use(express.static(path.join(BASE_DIR, 'dist')));

app.listen(4000, function () {
    console.log('App is running on http://127.0.0.1:' + APPLICATION_PORT);
});