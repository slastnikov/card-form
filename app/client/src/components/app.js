/**
 * Created by evgeny.slastnikov on 8/6/18.
 */

import React, {Component} from 'react';
import {Header} from "./header";
import {Content} from './content';

class App extends Component {
    render() {
        return (
            <section className='application'>
                <Header/>
                <Content/>
            </section>
        );
    }
}

export default App;
