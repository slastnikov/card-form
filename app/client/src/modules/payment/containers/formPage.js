/**
 * Created by evgeny.slastnikov on 9/19/18.
 */
import React, {Component} from "react";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import NumberFormat from 'react-number-format';

import * as FORMATS from "../configs/formPage/formats";
import * as TEXTS from "../configs/formPage/texts";
import * as PARAMS from "../configs/params";
import {APP_MODES} from "../configs/params";
import {payButtonClick} from "../actions/index";

class FormPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            cardNumber: '',
            issuerName: '',
            expirationDate: '',
            cvcCode: '',
            amount: 0
        }
    }

    formatAmountHint(amount = 0) {
        return TEXTS.AMOUNT.HINT
            .replace('%AMOUNT%', amount)
            .replace('%CURRENCY%', PARAMS.CURRENCY_CODE)
            .replace('%COMMISSION%', PARAMS.COMMISSION);
    }

    renderError(param) {
        return this.props.errors[param] ?
            <div className="payment-form-error">{this.props.errors[param]}</div> : '';
    }

    render() {
        const amount_hint = this.formatAmountHint(this.state.amount);
        const payment_system_logo = require('../styles/images/' + PARAMS.PAYMENT_SYSTEM + '.png');

        return (
            <form id="payment">
                <div className="paymentForm">

                    <div className="cardProps">
                        <label htmlFor="cardNumber" className="card-label">
                            {TEXTS.CARD_NUMBER}
                            <NumberFormat id="cardNumber"
                                          className="card-number"
                                          format={FORMATS.CARD_NUMBER}
                                          mask="_"
                                          onValueChange={(values) => {
                                              this.setState({cardNumber: values.value})
                                          }}
                            />
                        </label>
                        {this.renderError('CARD_NUMBER')}
                        <label htmlFor="cardNumber" className="card-label">
                            {TEXTS.ISSUER_NAME}
                            <input id="issuerName"
                                   className="issuer-name"
                                   onChange={(e) => {
                                              this.setState({issuerName: e.target.value})
                                          }}
                            />
                        </label>
                        {this.renderError('ISSUER_NAME')}
                        <label htmlFor="cardExpirationDate" className="card-label">
                            {TEXTS.EXPIRATION_DATE}
                            <NumberFormat id="cardExpirationDate"
                                          className="card-expiration-date"
                                          format={FORMATS.EXPIRATION_DATE}
                                          mask="_"
                                          onValueChange={(values) => {
                                              this.setState({expirationDate: values.value})
                                          }}
                            />
                        </label>
                        {this.renderError('EXPIRATION_DATE')}
                        <div className="card-cvc-with-hint">
                            <div className="card-cvc-input">
                                <label htmlFor="cardCVC" className="card-label">
                                    {TEXTS.CVC_CODE.LABEL}
                                    <NumberFormat id="cardCVC"
                                                  className="card-cvc"
                                                  format={FORMATS.CVC_CODE}
                                                  mask="*"
                                                  onValueChange={(values) => {
                                                      this.setState({cvcCode: values.value})
                                                  }}
                                    />
                                </label>
                            </div>
                            {this.renderError('CVC_CODE')}
                            <div className="card-cvc-hint">
                                {TEXTS.CVC_CODE.HINT}
                            </div>
                        </div>
                    </div>
                    <div className="payment-amount">
                        <div className="amount-input">
                            <NumberFormat
                                id="amount"
                                className="amount"
                                thousandSeparator={true}
                                prefix={'$'}
                                placeholder={TEXTS.AMOUNT.PLACEHOLDER}
                                onValueChange={(values) => {
                                    this.setState({amount: values.value})
                                }}
                            />
                        </div>
                        {this.renderError('AMOUNT')}
                        <div className="amount-hint">
                            {amount_hint}
                        </div>

                    </div>

                    <button className="btn btn-primary" onClick={(e) => {
                        e.preventDefault();
                        this.props.payBtnClick(this.state)
                    }}>
                        {TEXTS.PAY_BUTTON}
                    </button>
                    <div>
                        <div className="payment-system">
                            <img src={payment_system_logo}/>
                        </div>
                        <div className="payment-bank">
                            {PARAMS.PAYMENT_BANK}
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

function mapStateToProps(state) {
    return {
        appMode: state.app.mode,
        confirmationCode: state.app.confirmationCode,
        cardNumberError: state.mainForm.cardNumberError,
        expirationDateError: state.mainForm.expirationDateError,
        errors: state.mainForm.errors
    };
}

function matchDispatchToProps(dispatch) {
    return bindActionCreators({
        payBtnClick: payButtonClick
    }, dispatch);
}

export default connect(mapStateToProps, matchDispatchToProps)(FormPage);