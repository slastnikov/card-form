/**
 * Created by evgeny.slastnikov on 9/21/18.
 */

import React, {Component} from 'react'
import {Provider} from 'react-redux';
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

import PaymentEngine from "../containers/paymentEngine";
import reducers from "../reducers/index";
import '../styles/scss/index.scss';

export class Payment extends Component {
    constructor(props) {
        super(props);
        this.store = createStore(reducers, applyMiddleware(thunk));
    }

    render() {
        return (
            <Provider store={this.store}>
                <PaymentEngine/>
            </Provider>
        );
    }
}