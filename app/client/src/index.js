/**
 * Created by evgeny.slastnikov on 8/6/18.
 */

import React from 'react';
import {render} from 'react-dom';

import App from "./components/app";
import {BASIC_ERROR_ALERT} from './configs/messages';

const appWrapper = document.getElementById('app');

if (appWrapper) {
    render(
        <App/>,
        appWrapper
    );
}
else {
    alert(BASIC_ERROR_ALERT);
}