/**
 * Created by evgeny.slastnikov on 8/6/18.
 */

import React from "react";

import {Payment} from "../modules/payment";

export const Content = () => (
    <main className="container">
        <Payment/>
    </main>
);

