/**
 * Created by evgeny.slastnikov on 9/22/18.
 */

export const CARD_NUMBER = '#### #### #### ####';
export const EXPIRATION_DATE = '##/##';
export const CVC_CODE = '###';