/**
 * Created by evgeny.slastnikov on 9/22/18.
 */
import React from 'react';
import {connect} from "react-redux";

import FormPage from "./formPage";
import ConfirmationPage from "./confirmationPage";
import SuccessPage from "./successPage";
import {APP_MODES} from "../configs/params";

const renderOutput = (mode) => {
    switch (mode) {
        case APP_MODES.INITIALIZATION:
            return <FormPage/>;
        case APP_MODES.CONFIRMATION:
            return <ConfirmationPage/>;
        case APP_MODES.SUCCESS:
            return <SuccessPage/>;
    }
};

const PaymentEngine = (props) => <div className="payment">{renderOutput(props.appMode)}</div>;

function mapStateToProps(state) {
    return {
        appMode: state.app.mode
    };
}

export default connect(mapStateToProps)(PaymentEngine);