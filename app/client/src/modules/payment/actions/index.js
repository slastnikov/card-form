/**
 * Created by evgeny.slastnikov on 8/6/18.
 */
import {PAYMENT_ACTIONS} from "../configs/actions";
import {getRandomInt} from "../../../utils/math";
import {CONFIRMATION_CODE_LENGTH} from "../configs/params";
import {FORM_ERRORS} from "../configs/formPage/texts";

export const moveToConfirmation = (confirmationCode) => ({
    type: PAYMENT_ACTIONS.MOVE_TO_CONFIRMATION,
    payload: confirmationCode
});
export const displayFormErrors = () => ({type: PAYMENT_ACTIONS.DISPLAY_FORM_ERRORS});
export const moveToSuccess = () => ({type: PAYMENT_ACTIONS.MOVE_TO_SUCCESS});
export const displayCheckCodeError = () => ({type: PAYMENT_ACTIONS.DISPLAY_CHECK_ERRORS});
export const updateFormErrors = (errors) => ({
    type: PAYMENT_ACTIONS.UPDATE_FORM_ERRORS,
    payload: errors
});
export const updateFormData = (formData) => ({
    type: PAYMENT_ACTIONS.UPDATE_FORM_DATA,
    payload: formData
});

const isFormCorrect = (formData) => {
    // TODO: refactor and add missed checks
    let errors = {};

    if (!formData.cardNumber) {
        errors['CARD_NUMBER'] = FORM_ERRORS.CARD_NUMBER;
    }

    if(!formData.issuerName) {
        errors['ISSUER_NAME'] = FORM_ERRORS.ISSUER_NAME;
    }

    if (!formData.expirationDate) {
        errors['EXPIRATION_DATE'] = FORM_ERRORS.EXPIRATION_DATE;
    }

    if (!formData.cvcCode) {
        errors['CVC_CODE'] = FORM_ERRORS.CVC_CODE;
    }

    if (formData.amount <= 0) {
        errors['AMOUNT'] = FORM_ERRORS.AMOUNT;
    }

    return (Object.keys(errors).length !== 0) ? errors : true;
};

const addCodeToUrl = (confirmationCode) => {
    if (window.history.replaceState) {
        window.history.replaceState(null, null, `?code=${confirmationCode}`);
    }
};

export const payButtonClick = (formData) => dispatch => {
    const checkResult = isFormCorrect(formData);
    if (typeof checkResult === 'object') {
        dispatch(updateFormErrors(checkResult));
        dispatch(displayFormErrors());
    }
    else {
        dispatch(updateFormData(formData));
        const confirmationCode = getRandomInt(CONFIRMATION_CODE_LENGTH);
        dispatch(moveToConfirmation(confirmationCode));
        addCodeToUrl(confirmationCode);
    }
};

export const tryToConfirm = (code) => (dispatch, getState) => {
    const state = getState();
    if (isCodeValid(code, state)) {
        dispatch(moveToSuccess());
    }
};

const isCodeValid = (code, state) => code === state.app.confirmationCode;
