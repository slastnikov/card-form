const path = require("path");

const BASE_DIR = path.join(__dirname, '..');
const CLIENT_DIR = path.join(BASE_DIR, 'app', 'client');
const SERVER_DIR = path.join(BASE_DIR, 'app', 'server');
const API_DIR = path.join(SERVER_DIR, 'routes', 'api');
const MOCKS_DIR = path.join(SERVER_DIR, 'routes', 'mocks');

const APPLICATION_PORT = 4000;

module.exports = {
    BASE_DIR,
    CLIENT_DIR,
    SERVER_DIR,
    API_DIR,
    MOCKS_DIR,
    APPLICATION_PORT
};