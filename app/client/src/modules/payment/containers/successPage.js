/**
 * Created by evgeny.slastnikov on 9/19/18.
 */
import React from 'react';
import {connect} from "react-redux";
import * as TEXTS from '../configs/successPage/texts';
import * as PARAMS from "../configs/params";

const SuccessPage = (props) => (
    <div className="success">
        {
            TEXTS.MESSAGE
                .replace('%AMOUNT%', props.amount)
                .replace('%CURRENCY%', PARAMS.CURRENCY_CODE)
        }
    </div>
);

function mapStateToProps(state) {
    return {
        amount: state.mainForm.amount
    };
}

export default connect(mapStateToProps)(SuccessPage);