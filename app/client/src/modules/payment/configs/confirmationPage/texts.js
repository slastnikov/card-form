/**
 * Created by evgeny.slastnikov on 9/22/18.
 */

export const CONFIRMATION = 'Put your confirmation code here:';
export const CONFIRMATION_BUTTON = 'Verify';
export const ERROR_MESSAGE = 'Your code is invalid';