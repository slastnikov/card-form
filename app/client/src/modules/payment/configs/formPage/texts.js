/**
 * Created by evgeny.slastnikov on 9/21/18.
 */

export const CARD_NUMBER = 'card number';
export const EXPIRATION_DATE = 'expiry date';
export const ISSUER_NAME = 'issuer name';
export const CVC_CODE = {
    LABEL: 'cvc code',
    HINT: 'Three digits from the back of the card'
};
export const AMOUNT = {
    PLACEHOLDER: 'Amount to receive',
    HINT: 'Amount to pay %AMOUNT% %CURRENCY%, including %COMMISSION%% commission'
};
export const PAY_BUTTON = 'Pay';

export const FORM_ERRORS = {
    CARD_NUMBER: 'Please enter the card number',
    ISSUER_NAME: 'Please enter the issuer name',
    EXPIRATION_DATE: 'Please enter the expiry date',
    CVC_CODE: 'Please enter the cvc code',
    AMOUNT: 'Please enter the amount'
};