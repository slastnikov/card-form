let config = require("./webpack.config.base");

config.devtool = 'inline-source-map';

module.exports = config;