/**
 * Created by evgeny.slastnikov on 9/22/18.
 */
export const CONFIRMATION_CODE_LENGTH = 6;
export const CURRENCY_CODE = 'USD';
export const COMMISSION = 1.3;

export const PAYMENT_SYSTEM = "visa";
export const PAYMENT_BANK = "Powered by Alfa-bank";

export const APP_MODES = {
    INITIALIZATION: 'INITIALIZATION',
    CONFIRMATION: 'CONFIRMATION',
    SUCCESS: 'SUCCESS'
};