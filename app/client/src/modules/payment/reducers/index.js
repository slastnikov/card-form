import {combineReducers} from 'redux';
import {AppReducer} from './app';
import {MainFormReducer} from "./mainForm";
import {ConfirmationReducer} from "./confirmation";

const reducers = combineReducers({
    app: AppReducer,
    mainForm: MainFormReducer,
    confirmation: ConfirmationReducer
});

export default reducers;