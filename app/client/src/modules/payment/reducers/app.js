/**
 * Created by evgeny.slastnikov on 8/6/18.
 */
import {APP_MODES} from "../configs/params";
import {PAYMENT_ACTIONS} from "../configs/actions";

const defaultState = {
    mode: APP_MODES.INITIALIZATION,
    confirmationCode: null
};

export const AppReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAYMENT_ACTIONS.MOVE_TO_CONFIRMATION:
            return {
                ...state,
                mode: APP_MODES.CONFIRMATION,
                confirmationCode: action.payload
            };
        case PAYMENT_ACTIONS.MOVE_TO_SUCCESS:
            return {
                ...state,
                mode: APP_MODES.SUCCESS
            };
        case PAYMENT_ACTIONS.DISPLAY_CHECK_ERRORS:
        default:
            return state;
    }
};