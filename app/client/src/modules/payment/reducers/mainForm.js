/**
 * Created by evgeny.slastnikov on 9/21/18.
 */
import {PAYMENT_ACTIONS} from "../configs/actions";

const defaultState = {
    cardNumber: '',
    issuerName: '',
    expirationDate: '',
    cvCode: '',
    sum: '',
    errors: {}
};

export const MainFormReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAYMENT_ACTIONS.UPDATE_FORM_DATA:
            return {
                ...state,
                ...action.payload
            };
        case PAYMENT_ACTIONS.UPDATE_FORM_ERRORS:
            return {
                ...state,
                errors: action.payload
            };
        default:
            return state;
    }
};