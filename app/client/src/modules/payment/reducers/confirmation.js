/**
 * Created by evgeny.slastnikov on 8/6/18.
 */
import {PAYMENT_ACTIONS} from "../configs/actions";
import {ERROR_MESSAGE} from "../configs/confirmationPage/texts";

const defaultState = {
    errorMessage: ''
};

export const ConfirmationReducer = (state = defaultState, action) => {
    switch (action.type) {
        case PAYMENT_ACTIONS.DISPLAY_CHECK_ERRORS:
            return {
                ...state,
                errorMessage: ERROR_MESSAGE
            };
        default:
            return state;
    }
};
