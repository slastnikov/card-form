/**
 * Created by evgeny.slastnikov on 9/22/18.
 */

export const getRandomInt = (length) => ("" + Math.random()).substring(2, 2 + length);